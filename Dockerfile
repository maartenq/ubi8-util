FROM registry.access.redhat.com/ubi8/ubi
ENV USER maarten
ENV VERSION v0.0.3
LABEL maintainer="maartenq"
LABEL org.opencontainers.image.title="UBI8 Util"
LABEL org.opencontainers.image.description="UBI8 with some utils"
LABEL org.opencontainers.image.version="${VERSION}"
LABEL org.opencontainers.image.documentation="https://gitlab.com/maartenq/uti8-util"
LABEL org.opencontainers.image.source="https://gitlab.com/maartenq/uti8-util"
LABEL org.opencontainers.image.url="https://quay.io/repository/maartenq/ubi8-util"
LABEL io.openshift.tags="utils"
LABEL io.k8s.description="UBI8 Container with utils"
RUN yum install -y \
    diffutils \
    git \
    glibc-langpack-en \
    iproute \
    iputils \
    less \
    man-db \
    net-tools \
    nmap \
    nmap-ncat \
    openssl \
    procps-ng \
    sudo \
    vim \
&& yum clean all \
&& useradd -u 1001 -G wheel -s /bin/bash -p $(openssl passwd -6 password) ${USER} \
&& sed -i 's/^#\s*\(%wheel\s\+ALL=(ALL)\s\+NOPASSWD:\s\+ALL\)/\1/' /etc/sudoers
WORKDIR /home/${USER}
USER ${USER}
